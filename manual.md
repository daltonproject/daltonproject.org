---
layout: master
include: content
---

## Manual

<div class="row">
  <div class="col-md-12">
    <div class="embed-responsive embed-responsive-4by3">
      <iframe class="embed-responsive-item"
          src="https://daltonproject.readthedocs.io" allowfullscreen></iframe>
    </div>
  </div>
</div>
