The sources behind https://daltonproject.org.

Served using [Jekyll](https://jekyllrb.com) and
[GitLab pages](https://docs.gitlab.com/ee/user/project/pages/).
